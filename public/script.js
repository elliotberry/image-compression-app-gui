// ************************ Drag and drop ***************** //
let dropArea = document.getElementById("drop-area")

// Prevent default drag behaviors
;['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {

  dropArea.addEventListener(eventName, preventDefaults, false)   
  document.body.addEventListener(eventName, preventDefaults, false)
})

// Highlight drop area when item is dragged over it
;['dragenter', 'dragover'].forEach(eventName => {

  dropArea.addEventListener(eventName, highlight, false)
})

;['dragleave', 'drop'].forEach(eventName => {

  dropArea.addEventListener(eventName, unhighlight, false)
})

// Handle dropped files
dropArea.addEventListener('drop', handleDrop, false)

function preventDefaults (e) {
  e.preventDefault()
  e.stopPropagation()
}

function highlight(e) {
  dropArea.classList.add('highlight')
}

function unhighlight(e) {
  dropArea.classList.remove('active')
}

function handleDrop(e) {

  var dt = e.dataTransfer
  var files = dt.files

  handleFiles(files)
}





function handleFiles(files) {
  document.getElementById("status").innerText = "Processing..";
  files = [...files]
  
 // initializeProgress(files.length)
  files.forEach(uploadFile)
 // files.forEach(previewFile)
}


async function uploadFile(file, i) {
 let resp = await fetch('http://localhost:3000/', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({path: file.path})
  });
  let funcresp = await resp.text();
  document.getElementById("status").innerText = "Processed. Waiting...";
  document.getElementById("output").innerText = document.getElementById("output").innerText + `${funcresp}\n`

}