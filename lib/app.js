const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');

const compress = require('./compress')
const app = express();



app.use(bodyParser.json());



app.use('/', express.static(path.join(__dirname, '../public')))

app.post('/', async function (req, res) {
 
    console.log(`got path: ${req.body.path}`)
    if (req.body && req.body.path) {
      let ret = await compress(req.body.path)
      res.send(ret)
    }
    else {
      res.send("need a path lol")
    }
})

app.listen(3000, () => console.log(`Express app listening on port 3000`));


module.exports = app;
