const imagemin = require("imagemin");
const path = require("path");

const imageminPngquant = require("imagemin-pngquant");
const imageminMozjpeg = require("imagemin-mozjpeg");
const imageminGifsicle = require("imagemin-gifsicle");
const imageminSvgo = require("imagemin-svgo");
const slugify = require("./slugify");
const increment = require("add-filename-increment");
const homedir = require("os").homedir();
const fs = require('graceful-fs');
const humanread = require("human-readable");



const hr = humanread.sizeFormatter({
  std: "JEDEC", // 'SI' (default) | 'IEC' | 'JEDEC'
  decimalPlaces: 2,
  keepTrailingZeroes: false,
  render: (literal, symbol) => `${literal} ${symbol}B`,
});



const tempDirectory = `/tmp/imageout`;
const finalDirectory = `${homedir}/Desktop`;

/*
resize example

sharp(input)
  .resize({ width: 100 })
  .toBuffer()
  .then(data => {
    // 100 pixels wide, auto-scaled height
  });

  */

//check if it's an img.
const checkExtension = function (path) {};


const readToBuffer = async function(filePath) {
  fs.readFile(filePath, function(err, buffer){

  })
} 

async function imageMagic(src) {
  let dataBuffer = await readFile(sourcePath);
  const {ext} = await FileType.fromBuffer(data);

  const data = await imagemin(
    dataBuffer,
    [
      imageminGifsicle({
        interlaced: true,
      }),
      imageminPngquant({
        quality: [0.6, 0.8],
      }),
      imageminMozjpeg(),
      imageminSvgo({
        plugins: [
          {
            removeViewBox: true,
          },
          {
            cleanupIDs: false,
          },
        ],
      }),
    ]
  );
  return data;
}

function getFileSize(filename) {
  const stats = fs.statSync(filename);
  return stats.size;
}

async function copyToFinalLocation(tempFilePath) {
  let parsed = path.parse(tempFilePath);
  let cleanBaseName = `${slugify(parsed.name)}${parsed.ext}`;
  // let cleanedTempPath = `${parsed.dir}/${cleanBaseName}`
  // if (tempFilePath !== cleanedTempPath) {
  //  fs.renameSync(tempFilePath, cleanedTempPath);
  // }
  let finalLocation = `${finalDirectory}/${cleanBaseName}`;
  let safeFinalLocation = increment(finalLocation, { platform: "darwin" });
  fs.copyFileSync(tempFilePath, safeFinalLocation);
  fs.unlinkSync(tempFilePath);
  return safeFinalLocation;
}

const elapsed = function (start) {
  let ret;
  var end = process.hrtime(start); // end[0] is in seconds, end[1] is in nanoseconds
  const timeInMs = (end[0] * 1000000000 + end[1]) / 1000000;
  if (timeInMs > 1000) {
    return `${(timeInMs / 1000).toFixed(4)}s`;
  } else {
    return `${timeInMs.toFixed(4)}ms`;
  }
};

async function compress(src) {
  let startTime = process.hrtime();
  let initialSize = getFileSize(src);
  if (!fs.existsSync(tempDirectory)) {
    fs.mkdirSync(tempDirectory);
  }
  let files = await imageMagic(src);

  let tempFilePath = files[0].destinationPath;
  let finalLocation = await copyToFinalLocation(tempFilePath);
  let finalSize = getFileSize(finalLocation);
  let savedBytes = hr(finalSize - initialSize);

  return `File output to ${finalLocation}. initial size: ${hr(
    initialSize
  )}. Compressed size: ${hr(finalSize)}. We saved ${hr(
    Math.abs(savedBytes)
  )}! Took ${elapsed(startTime)}`;
}


module.exports = compress;
